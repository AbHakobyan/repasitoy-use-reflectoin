﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Repasitory_Reflection.Models
{
    class People
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public byte Age { get; set; }
        public City City { get; set; }
        public string Country { get; set; }

        public string Information => $"{Name} {Surename} {Age} {City} {Country}";

        public override string ToString() => Information;
    }
}
