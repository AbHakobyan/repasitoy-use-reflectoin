﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_Repasitory_Reflection.Models;
using System_Repasitory_Reflection.Repasitory;

namespace System_Repasitory_Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = CreatPeople(5).ToArray();
            var list = new People_Repasitory();
            //list.AddRange(people);
            list.SaveChanges();

            List<People> peoples = list.AsEnumerable().ToList();
            foreach (var item in peoples)
            {
                Console.WriteLine(item);
            }
        }

        static IEnumerable<People> CreatPeople(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new People
                {
                    Name = $"P{1}",
                    Surename = $"P{1}yan",
                    Age = (byte)rnd.Next(0, 70),
                    City = (City)rnd.Next(0, 5),
                    Country = "Armenia"
                };
            }
        }
    }
}
