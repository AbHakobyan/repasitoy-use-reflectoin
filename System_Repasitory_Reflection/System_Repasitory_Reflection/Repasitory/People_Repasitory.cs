﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_Repasitory_Reflection.Models;

namespace System_Repasitory_Reflection.Repasitory
{
    class People_Repasitory : Base_Repasitory<People>
    {
        protected override string Path => "People.txt";

        protected override bool IsIgnored(string propName)
        {
            if (propName == nameof(People.Information))
            {
                return true;
            }
            return false;
        }
    }
}
