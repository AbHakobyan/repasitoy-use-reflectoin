﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System_Repasitory_Reflection.Repasitory
{
    abstract class Base_Repasitory<T> : IBase_Repasitory<T> where T : class,new()
    {
        protected abstract string Path { get; }

        public Base_Repasitory()
        {
            _list = AsEnumerable().ToList();
        }

        List<T> _list;

        public void Add(T model)
        {
            _list.Add(model);
        }

        public void AddRange(IEnumerable<T> model)
        {
            foreach (var item in model)
            {
                _list.Add(item);
            }
        }

        public void Insert(int index, T model)
        {
            _list.Insert(index, model);
        }

        public int SaveChanges()
        {
            int count = 0;
            StreamWriter writer = new StreamWriter(Path, true, Encoding.Default);
            foreach (var list in _list)
            {
                writer.WriteLine("{");
                WriteModel(writer, list);
                writer.WriteLine("}");
            }
            _list.Clear();
            writer.Dispose();
            return count;
        }

        private void WriteModel(StreamWriter writer, T model)
        {
            var propInfo = model.GetType().GetProperties();
            foreach (var pI in propInfo)
            {
                if (IsIgnored(pI.Name))
                {
                    continue;
                }
                object value = pI.GetValue(model);
                if (pI.PropertyType.IsEnum)
                {
                    writer.WriteLine($"{pI.Name}:{Convert.ToString(value)}");
                }
                else
                {
                    writer.WriteLine($"{pI.Name}:{value}");
                }
            }
        }

        public IEnumerable<T> AsEnumerable()
        {
            if (!File.Exists(Path))
                File.Create(Path).Dispose();

            StreamReader reader = new StreamReader(Path, Encoding.Default);
            T model = null;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        model = new T();
                        break;
                    case "}":
                        yield return model;
                        break;
                    default:
                        ReadModel(line, model);
                        break;
                }
            }
            reader.Dispose();
        }

        private void OnFillModel(string[] data, T model)
        {
            string propName = data[0];
            var pI = model.GetType().GetProperty(propName, BindingFlags.Instance | BindingFlags.Public);
            if (pI != null)
            {
                if (pI.PropertyType.IsEnum)
                {
                    object values = Enum.Parse(pI.PropertyType, data[1]);
                    pI.SetValue(model, values);
                }
                else
                {
                    object value = Convert.ChangeType(data[1], pI.PropertyType);
                    pI.SetValue(model, value);
                }
            }
        }

        public void ReadModel(string line, T model)
        {
            string[] data = line.Split(':');
            if (data.Length < 2)
            {
                return;
            }
            OnFillModel(data, model);
        }



        protected abstract bool IsIgnored(string propName);
    }
}
